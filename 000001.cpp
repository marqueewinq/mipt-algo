#include <iostream>
#include <vector>

using namespace std;

vector <int> buildPrefixFunction(const string & s)
{
	vector <int> pi(s.length());
	// pi[0] = 0; // строка служит понятийным целям; чтобы, читая код, человек заметил, что цикл с единицы. Впрочем, технически её действительно можно опустить.
	for (size_t i = 1; i < s.length(); i++)
	{
		int k = pi[i - 1];
		while (k > 0 && s[i] != s[k])
			k = pi[k - 1];
		if (s[i] == s[k])
			k++;
		pi[i] = k;
	}
	
	return pi;
}


int main()
{
	ios_base::sync_with_stdio(false);
	string s;
	// Всё еще не понял, почему у меня написано плохо.
	while (getline(cin, s))
	{
		if (s.empty())
			break;
		vector <int> pi = pf(s);
		for_each(pi.begin() + 1, pi.end(), [](int value) { cout << value << " "; });
		cout << "\n";
	}
}
